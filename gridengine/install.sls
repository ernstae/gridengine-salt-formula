{% import_yaml slspath + "/defaults.yaml" as defaults %}
{% set gridengine = salt['pillar.get']('gridengine', defaults, merge=True) %}

# make control directory (/etc/gridengine-salt-formula)
gridengine_create_staging_directory:
  file.directory:
    - name: {{ gridengine.installer.staging_dir }}
    - mode: 755
    - user: root
    - group: root

# drop the auto-installer template configuration file in the directory
gridengine_auto_installer_template:
  file.managed:
    - name: {{ gridengine.installer.staging_dir }}/auto_installer_template
    - source: 'salt://{{ slspath }}/files/auto_installer_template.jinja'
    - template: jinja
    - user: root
    - context:
      - gridengine

# deploy the tarballs to the minion
{% for component, url in gridengine.installer.components.all.iteritems() %}
gridengine_install_component_{{ component }}:
  archive.extracted:
    - name: {{ gridengine.installer.staging_dir }}/{{ component }}
    - source: {{ url }}
{% if gridengine.installer.check_source_hash is True %}
    - source_hash: {[ url ]}.sig
{% endif %}
{% endfor %}

# install using the configuration file interpolated template


