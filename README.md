gridengine-salt-formula
=====
This salt formula is intended to simplify the installation of GridEngine cluster schedulers and compute nodes.
The intent is to provide an extensible formula for use in Salt allowing for arbitrary use in any generic environment.
This is tested under Ubuntu 14.04 but should be platform agnostic.

Author: Andrew Ernst <ernstae@github.com>

Copyright: Andrew Ernst, University of Washington Institute for Health Metrics and Evaluation (c)2017

Warning
====
This is a work in progress and should not be considered ready for production at this time.

Example Pillar Data
---
In the pillar, the `config` section maps one-to-one with the key/value pairs that inst_sge expects.  However, the variable name in the pillar is in lower case.  When the jinja template is interpolated, the variables are again returned to upper case characters.
```
gridengine:
  installer:
    staging_dir: /etc/gridengine-salt-formula
    template: auto_install_template
  config:
    sge_root: "/vagrant/UGE"
    sge_qmaster_port: "802"
    sge_execd_port: "803"
    sge_enable_smf: "false"
    sge_cluster_name: "p802"
    sge_jmx_port: "Please enter port"
    sge_jmx_ssl: "false"
    sge_jmx_ssl_client: "false"
    sge_jmx_ssl_keystore: "Please enter absolute path of server keystore file"
    sge_jmx_ssl_keystore_pw: "Please enter the server keystore password"
    sge_jvm_lib_path: "Please enter absolute path of libjvm.so"
    sge_additional_jvm_args: "-Xmx256m"
    cell_name: "default"
    admin_user: ""
    qmaster_spool_dir: "/vagrant/UGE/default/spool/master"
    execd_spool_dir: "/UGEexecdspool"
    gid_range: "20000-20100"
    spooling_method: "classic"
    db_spooling_server: "none"
    db_spooling_dir: "/opt/tools/uge/mycluster/bdb"
    pg_spooling_args: "none"
    par_execd_inst_count: "20"
    admin_host_list: "execd1 execd2 master"
    submit_host_list: "execd1 execd2 master"
    exec_host_list: "execd1 execd2 master"
    execd_spool_dir_local: ""
    hostname_resolving: "true"
    shell_name: "ssh"
    copy_command: "scp"
    default_domain: "none"
    admin_mail: "none"
    add_to_rc: "true"
    set_file_perms: "true"
    reschedule_jobs: "wait"
    schedd_conf: "3"
    shadow_host: ""
    exec_host_list_rm: ""
    remove_rc: "false"
    windows_support: "false"
    win_admin_name: "Administrator"
    win_domain_access: "false"
    win_admin_password: ""
    csp_recreate: "true"
    csp_copy_certs: "false"
    csp_country_code: "DE"
    csp_state: "Germany"
    csp_location: "Building"
    csp_orga: "Organisation"
    csp_orga_unit: "Organisation_unit"
    csp_mail_address: "name@yourdomain.com"
    sge_reader_threads: "2"
```
Installer
----
During the installation, you can optionally choose to enable the hash check of the download files for the software tarballs.  This is handled by setting `gridengine.installer.check_source_hash: True` which will look for checksum files with the extension `.sig` appended to the download URL.

Given this pillar data:
```
gridengine:
  installer:
    check_source_hash: True
    component:
      arco: https://ernst.ca/uge-arco-latest.tar.gz
```
The formula would attempt to download the file `http://ernst.ca/uge-arco-latest.tar.gz.sig` and verify the hash against the downloaded bits on the minion.


Acknowledgements
----
The default pillar example was derived from @dgruber vagrantGridEngine
* https://github.com/dgruber/vagrantGridEngine

